package com.pligor.inappgoogle

import play.api.libs.json._
import play.api.libs.json.JsString

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 *
 * include this inside manifest:
 * <uses-permission android:name="com.android.vending.BILLING"/>
 *
 * create package com.android.vending.billing
 *
 * Copy the IInAppBillingService.aidl file from <sdk>/extras/google/play_billing/
 * and paste it into the src/com.android.vending.billing/ folder in your workspace.
 *
 *
 * If we want to support In App Billing V2 we should check into that: http://developer.android.com/google/play/billing/versions.html
 *
 * sku id and item id and product id are the same thing
 *
 * YOU CAN ONLY CANCEL A SUBSCRIPTION THIS WAY: https://developers.google.com/android-publisher/v1/purchases/cancel#auth
 */
object InAppBilling {
  val intentAction = "com.android.vending.billing.InAppBillingService.BIND"
  val apiVersion = 3
  val skuListKey = "ITEM_ID_LIST"
  val responseCodeKey = "RESPONSE_CODE"
  val detailsListKey = "DETAILS_LIST"
  val buyIntentKey = "BUY_INTENT"
  val inAppPurchaseItemListKey = "INAPP_PURCHASE_ITEM_LIST"
  val inAppPurchaseDataListKey = "INAPP_PURCHASE_DATA_LIST"
  val inAppDataSignatureKey = "INAPP_DATA_SIGNATURE"
  val inAppContinuationTokenKey = "INAPP_CONTINUATION_TOKEN"
  val inAppPurchaseDataKey = "INAPP_PURCHASE_DATA"

  object ResponseCode extends Enumeration {
    val INVALID = -1

    val BILLING_RESPONSE_RESULT_OK = Value(0)
    val BILLING_RESPONSE_RESULT_USER_CANCELED = Value(1)
    val BILLING_RESPONSE_RESULT_BILLING_UNAVAILABLE = Value(3)
    val BILLING_RESPONSE_RESULT_ITEM_UNAVAILABLE = Value(4)
    val BILLING_RESPONSE_RESULT_DEVELOPER_ERROR = Value(5)
    val BILLING_RESPONSE_RESULT_ERROR = Value(6)
    val BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED = Value(7)
    val BILLING_RESPONSE_RESULT_ITEM_NOT_OWNED = Value(8)
  }

  object Type extends Enumeration {
    val inapp = Value("inapp")
    val subs = Value("subs"); //subscriptions

    implicit val jsonWritesInAppBillingTypeValue = new Writes[InAppBilling.Type.Value] {
      def writes(obj: InAppBilling.Type.Value): JsValue = JsString(obj.toString)
    }

    implicit val jsonReadsInAppBillingTypeValue = new Reads[InAppBilling.Type.Value] {
      def reads(json: JsValue): JsResult[Type.Value] = json.asOpt[JsString].fold(ifEmpty = {
        JsError("").asInstanceOf[JsResult[InAppBilling.Type.Value]]
      })({
        jsString =>
          JsSuccess(InAppBilling.Type.withName(jsString.value))
      })
    }
  }

}