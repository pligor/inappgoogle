package com.pligor.inappgoogle

import play.api.libs.json.Json

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object PurchaseData {
  /*private implicit val jsonFormatPurchaseStateValue = new JsonFormat[PurchaseState.Value] {
    def write(obj: PurchaseState.Value): JsValue = JsNumber(obj.id);

    def read(json: JsValue) = PurchaseState(json.asInstanceOf[JsNumber].value.toInt);
  }*/

  /*implicit val itsJsonFormat = jsonFormat[String,
    String,
    String,
    Long,
    Short,//PurchaseState.Value,
    String,
    String,
    PurchaseData
    ](
    PurchaseData.apply,
    "orderId",
    "packageName",
    "productId",
    "purchaseTime",
    "purchaseState",
    "developerPayload",
    "purchaseToken"     //IF YOU WANT YOU CAN SAVE THIS ONE INSIDE DATABASE SO YOU CAN CONSUME LATER
  );*/

  implicit val itsJsonFormat = Json.format[PurchaseData]
}


case class PurchaseData(
                         orderId: String,
                         packageName: String,
                         productId: String,
                         purchaseTime: Long,
                         purchaseState: Short,//PurchaseState.Value,
                         developerPayload: String,
                         purchaseToken: String
                         );
