Some helper classes and trait to help you work with google in app purchases.
You may use InAppBillingActivity for activities or InAppBillingTrait if you are in a context like a receiver where you could consume a product or check for a purchase without being able to purchase it.

###Dependencies###
* "com.typesafe.play" %% "play-json" % "2.2.0"
* com.pligor.generic

Used in the following projects:

* [bman](http://bman.co)
* [Fcarrier](http://facebook.com/FcarrierApp)