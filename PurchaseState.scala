package com.pligor.inappgoogle

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object PurchaseState extends Enumeration {
  val purchased = Value(0)

  val canceled = Value(1)

  val refunded = Value(2)

  def resolveFallback(index: Int) = index match {
    case 0 => purchased
    case 1 => canceled
    case 2 => refunded
  }
}
