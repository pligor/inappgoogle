package com.pligor.inappgoogle

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object PurchaseType extends Enumeration {
  val inapp = Value

  //YOU CAN ONLY CANCEL A SUBSCRIPTION THIS WAY: https://developers.google.com/android-publisher/v1/purchases/cancel#auth
  val subs = Value
}
