package com.pligor.inappgoogle

import android.content.Intent
import java.util.UUID
import android.app.{Activity, PendingIntent}

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
trait InAppBillingActivity extends Activity with InAppBillingTrait {
  //abstract

  protected def inAppPurchaseRequestCode: Int

  protected def onInAppPurchaseSuccess(): Unit

  protected def onInAppPurchaseFailed(): Unit

  protected def onInAppPurchaseAlreadyOwned(): Unit

  //concrete

  private var randomRequestUUID: Option[String] = None

  override def onStop(): Unit = {
    super.onStop()

    terminateInAppBillingService(this)
  }

  override def onActivityResult(requestCode: Int, resultCode: Int, data: Intent): Unit = {
    def propagateRequest(): Unit = {
      super.onActivityResult(requestCode, resultCode, data)
    }

    if (Option(data).isDefined) {
      if (requestCode == inAppPurchaseRequestCode) {
        val responseCode = data.getIntExtra(InAppBilling.responseCodeKey, InAppBilling.ResponseCode.INVALID)
        val purchaseDataString = data.getStringExtra(InAppBilling.inAppPurchaseDataKey)
        val dataSignature = data.getStringExtra(InAppBilling.inAppDataSignatureKey)

        /*if (responseCode == InAppBilling.ResponseCode.INVALID || resultCode != Activity.RESULT_OK) {
          onInAppPurchaseFailed();
        } else {*/
        val responseCodeOption = try {
          Some(InAppBilling.ResponseCode(responseCode))
        } catch {
          case e: NoSuchElementException => None
        }

        if (responseCodeOption.isDefined) {
          responseCodeOption.map {
            case InAppBilling.ResponseCode.BILLING_RESPONSE_RESULT_OK => {
              assert(randomRequestUUID.isDefined, "the developer payload must already have been defined by now. If you see this maybe the activity stopped and restarted that is why you see a None here")

              val purchaseDataOption = parsePurchaseData(purchaseDataString)

              if (purchaseDataOption.isDefined && (purchaseDataOption.get.developerPayload == randomRequestUUID.get)) {
                onInAppPurchaseSuccess()
              } else {
                onInAppPurchaseFailed()
              }
            }
            case InAppBilling.ResponseCode.BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED => {
              onInAppPurchaseAlreadyOwned()
            }
            case _ => {
              onInAppPurchaseFailed()
            }
          }
        } else {
          onInAppPurchaseFailed()
        }

        //log log "responseCode in activity result is: " + responseCode
      } else {
        propagateRequest()
      }
    } else {
      propagateRequest()
    }
  }

  def purchaseItem(itemId: String,
                   purchaseType: PurchaseType.Value,
                   onItemAlreadyOwned: => Unit,
                   onError: (InAppBilling.ResponseCode.Value) => Unit
                    ): Unit = {
    //send a random uuid only to get it back later and verify it. somehow this plays a role in security, dont know how exactly
    val developerPayload = {
      randomRequestUUID = Some(UUID.randomUUID().toString)
      randomRequestUUID.get
    }

    executeInAppTask(
    {
      serviceInterface =>
        serviceInterface.getBuyIntent(
          InAppBilling.apiVersion,
          getPackageName,
          itemId,
          purchaseType.toString,
          developerPayload
        )
    }, {
      bundle =>
        val response = bundle.getInt(InAppBilling.responseCodeKey)

        InAppBilling.ResponseCode(response) match {
          case InAppBilling.ResponseCode.BILLING_RESPONSE_RESULT_OK => {
            //log log "response code is: " + InAppBilling.ResponseCode(response)
            val pendingIntent = bundle.getParcelable[PendingIntent](InAppBilling.buyIntentKey)

            startIntentSenderForResult(
            pendingIntent.getIntentSender,
            inAppPurchaseRequestCode /*RequestCode.IN_APP_PURCHASE.id*/ , {
              val fillInIntent = new Intent
              fillInIntent
            }, {
              val flagsMask = 0
              flagsMask
            }, {
              val flagsValues = 0
              flagsValues
            }, {
              val extraFlags = 0
              extraFlags
            }
            )
          }
          case InAppBilling.ResponseCode.BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED => {
            //log log "response code is: " + InAppBilling.ResponseCode(response)
            onItemAlreadyOwned
            //http://iserveandroid.blogspot.gr/2011/03/how-to-launch-pending-intent.html
            /*val pendingIntent = new PendingIntent;
            PendingIntent.getActivity(
              this,
              RequestCode.IN_APP_PURCHASE.id, new Intent

                sendPendingIntent(pendingIntent, fillInIntent = new Intent);
            startIntentSenderForResult()*/
          }
          case _ => {
            //log log "starting a safePurchase request has failed with code: " + response + " " + InAppBilling.ResponseCode(response)
            onError(InAppBilling.ResponseCode(response))
          }
        }
    }
    //)
    )
  }
}
