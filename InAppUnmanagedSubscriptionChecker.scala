package com.pligor.inappgoogle

import android.content.Context

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
trait InAppUnmanagedSubscriptionChecker extends InAppBillingTrait {
  //abstract

  //can be implemented as vals

  protected def inAppPurchaseType: PurchaseType.Value

  protected def inAppProductId: String

  //must NOT be implemented as vals

  protected def afterUnsuccessfulInAppBillingCheck(implicit context: Context): Unit

  protected def afterSuccessfulInAppBillingCheck(implicit context: Context): Unit

  protected def onNotPurchased(implicit context: Context): Unit

  protected def onPurchaseVerified(purchaseData: PurchaseData)(implicit context: Context): Unit

  protected def isInAppSubscribed(implicit context: Context): Boolean

  //concrete

  protected def checkInAppPurchase(implicit context: Context): Unit = {
    bindInAppBillingService(onInAppBillingServiceConnectionFailure = {
      afterUnsuccessfulInAppBillingCheck
    })
  }

  protected def onAfterInAppBillingServiceIsConnected(implicit context: Context): Unit = {
    checkInAppPurchase({
      isPurchasedOption =>  //never mind for this value
        if (isInAppSubscribed) {
          //log log "in app is subscribed"

          terminateInAppBillingService

          afterSuccessfulInAppBillingCheck
        } else {

          consumePurchase(inAppProductId, inAppPurchaseType)(
            onConsumeSuccess = {
              //log log "consume is successful. nothing to show to the user here"

              terminateInAppBillingService

              afterUnsuccessfulInAppBillingCheck
            },
            onConsumeError = {
              errorCode =>
                //log log "consume error: " + errorCode.toString

                terminateInAppBillingService

                afterUnsuccessfulInAppBillingCheck
            },
            onNoPurchasedItemFound = {
              //log log "there is no related item"

              terminateInAppBillingService

              afterUnsuccessfulInAppBillingCheck
            }
          )

        }
    })

    //log log "executed in app subscription refresher task"
  }

  private def checkInAppPurchase(op: Option[Boolean] => Unit)(implicit context: Context): Unit = {
    queryPurchasedItems(inAppPurchaseType)(
      onSuccess = {
        (ownedSkus, purchaseDatas) => {
          //log log "owned skus: " + (if (ownedSkus.isEmpty) "empty" else ownedSkus.mkString(", "))

          val purchaseDataOption = purchaseDatas.find(pd =>
            pd.productId == inAppProductId &&
              pd.packageName == context.getPackageName
          )

          val isPurchased = purchaseDataOption.isDefined

          if (isPurchased) {
            val purchaseData = purchaseDataOption.get

            //find out about that. what does it produce? and also the lines above
            //log log "PurchaseState: " + PurchaseState(purchaseData.purchaseState.toInt)

            onPurchaseVerified(purchaseData)
          } else {
            //the item id is verified that is not purchased
            //if we have it already cleared then we are in agreement
            //else if it has a value then we must clear it
            //let's clear it either way

            onNotPurchased
          }

          op(Some(isPurchased))
        }
      },
      onError = {
        responseCode =>
          //log log "do nothing if it is empty, leave it empty, if has timestamp, no need to do anything"

          op(None)
      }
    )
  }
}
