package com.pligor.inappgoogle

import com.android.vending.billing.IInAppBillingService
import android.content.{Context, Intent, ComponentName, ServiceConnection}
import android.os.{Bundle, IBinder}
import scala.collection.JavaConverters._
import java.util
import scala.collection.mutable
import scala.concurrent.Future
import com.pligor.generic.FutureHelper.interruptableFuture
import scala.concurrent.ExecutionContext.Implicits.global
import play.api.libs.json.Json

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 *
 * sku id and item id and product id are the same thing
 *
 * YOU CAN ONLY CANCEL A SUBSCRIPTION THIS WAY: https://developers.google.com/android-publisher/v1/purchases/cancel#auth
 *
 * If we want to support In App Billing V2 we should check into that: http://developer.android.com/google/play/billing/versions.html
 */
trait InAppBillingTrait {
  //abstract

  protected def onAfterInAppBillingServiceIsConnected(implicit context: Context): Unit

  //concrete

  private var inAppBillingServiceInterface: Option[IInAppBillingService] = None

  private val inAppTasks = mutable.Buffer.empty[() => Boolean]

  protected var isInAppBillingServiceBound: Option[Boolean] = None

  private var inAppBillingServiceConnection: Option[ServiceConnection] = None

  private def setInAppBillingServiceConnection(implicit context: Context): ServiceConnection = {
    inAppBillingServiceConnection = Some(new ServiceConnection {
      def onServiceConnected(componentName: ComponentName, boundService: IBinder): Unit = {
        inAppBillingServiceInterface = Some(IInAppBillingService.Stub.asInterface(boundService))

        //log log "in app billing service is connected"

        onAfterInAppBillingServiceIsConnected
      }

      def onServiceDisconnected(componentName: ComponentName): Unit = {
        inAppBillingServiceInterface = None
      }
    })

    inAppBillingServiceConnection.get
  }

  protected def executeInAppTask(backgroundOp: (IInAppBillingService) => Bundle, op: (Bundle) => Unit /*inAppTask: InAppTask*/) = {
    /*inAppTasks += inAppTask
    inAppTask.execute(inAppBillingServiceInterface.get)*/

    val (f, cancellor) = interruptableFuture {
      future: Future[Unit] =>

        op(
          backgroundOp(inAppBillingServiceInterface.get)
        )
    }

    inAppTasks += cancellor
  }

  /**
   * typically called in on start of an activity
   */
  protected def bindInAppBillingService(onInAppBillingServiceConnectionFailure: => Unit)
                                       (implicit context: Context): Boolean = {
    //http://stackoverflow.com/questions/8353969/android-cannot-bind-to-service-in-app-billing
    //log log "binding in app billing service"

    isInAppBillingServiceBound = Some(
      context.getApplicationContext.bindService(new Intent(InAppBilling.intentAction), setInAppBillingServiceConnection, Context.BIND_AUTO_CREATE)
    )

    if (isInAppBillingServiceBound.get) {
      //nop everything will be taken care of another handler
    } else {
      onInAppBillingServiceConnectionFailure
    }

    isInAppBillingServiceBound.get
  }

  /**
   * typically called in on stop of an activity
   */
  protected def terminateInAppBillingService(implicit context: Context): Unit = {
    inAppBillingServiceInterface.map {
      dontcare =>
        if (isInAppBillingServiceBound.get && inAppBillingServiceConnection.isDefined) {
          try {
            context.unbindService(inAppBillingServiceConnection.get)
          } catch {
            case e: IllegalArgumentException => //nop
          }
        } else {
          //no need to unbind anything
        }

        inAppBillingServiceInterface = None //because the onServiceDisconnected gets called only on very rare occasions (this is by google's design)

        //log log "in app billing service is disconnected"
    }

    isInAppBillingServiceBound = None

    inAppBillingServiceConnection = None

    inAppTasks foreach (_.apply())
  }

  def consumePurchase(itemId: String, purchaseType: PurchaseType.Value)(
    onConsumeSuccess: => Unit,
    onConsumeError: InAppBilling.ResponseCode.Value => Unit,
    onNoPurchasedItemFound: => Unit)(implicit context: Context): Unit = {

    queryPurchasedItems(purchaseType)(
      onSuccess = {
        (skus, purchaseDatas /*, signatures*/) =>
          val purchaseDataOption = purchaseDatas.find(_.productId == itemId)
          if (purchaseDataOption.isDefined) {
            consumePurchase(itemId, purchaseDataOption.get.purchaseToken)(onConsumeSuccess, onConsumeError)
          } else {
            //log log "no purchased item found with the given item id"
            onNoPurchasedItemFound
          }
      },
      onError = {
        responseCode =>
          onConsumeError(responseCode)
      })
  }

  def consumePurchase(itemId: String, purchaseToken: String)(
    onSuccess: => Unit,
    onError: InAppBilling.ResponseCode.Value => Unit
    )(implicit context: Context): Unit = {

    val myConsumeResponseKey = "DUMMY_CONSUME_RESPONSE_KEY"
    executeInAppTask(
    {
      serviceInterface =>
        val bundle = new Bundle()
        bundle.putInt(myConsumeResponseKey,
          serviceInterface.consumePurchase(
            InAppBilling.apiVersion,
            context.getPackageName,
            purchaseToken
          )
        )
        bundle
    }, {
      bundle =>
        val consumeResponse = bundle.getInt(myConsumeResponseKey)
        //log log "consumeResponse " + consumeResponse
        InAppBilling.ResponseCode(consumeResponse) match {
          case InAppBilling.ResponseCode.BILLING_RESPONSE_RESULT_OK => onSuccess
          case x: InAppBilling.ResponseCode.Value => onError(x)
        }
    }
    )
  }

  def queryItemsAvailableForPurchase(itemIds: Seq[String])
                                    (purchaseType: PurchaseType.Value)
                                    (onSuccess: Seq[SkuInfo] => Unit,
                                     onFailure: InAppBilling.ResponseCode.Value => Unit = (dummy: InAppBilling.ResponseCode.Value) => {}
                                      )(implicit context: Context): Unit = {
    executeInAppTask(
    {
      serviceInterface =>
        serviceInterface.getSkuDetails(
        InAppBilling.apiVersion,
        context.getPackageName,
        purchaseType.toString, {
          val querySkus = new Bundle
          querySkus.putStringArrayList(InAppBilling.skuListKey, new util.ArrayList[String](itemIds.asJavaCollection))
          querySkus
        }
        )
    }, {
      bundle =>
        val response = bundle.getInt(InAppBilling.responseCodeKey)
        InAppBilling.ResponseCode(response) match {
          case InAppBilling.ResponseCode.BILLING_RESPONSE_RESULT_OK => {
            //log log "the response code is (" + response + "): " + InAppBilling.ResponseCode(response)

            val responseList = bundle.getStringArrayList(InAppBilling.detailsListKey)

            //if responseList is null or empty then responseList.asScala is an empty iterable
            /*responseList.asScala foreach {
              responseDetails =>
                log log "response details is this: " + responseDetails;
            }*/

            val responseJsons = responseList.asScala.map(responseString => try {
              Some(Json.parse(responseString))
            } catch {
              case e: org.codehaus.jackson.JsonParseException => None
            }).filter(_.isDefined).map(_.get)

            val skuInfos = responseJsons.map({
              responseJson =>
                Json.fromJson(responseJson)(SkuInfo.itsJsonFormat).asOpt
            }).filter(_.isDefined).map(_.get)

            onSuccess(skuInfos)
          }
          case x => {
            //log log "the response code was (" + response + "): " + InAppBilling.ResponseCode(response)

            onFailure(x)
          }
        }
    }
    )
  }


  def queryPurchasedItems(purchaseType: PurchaseType.Value)
                         (//we will deal with the signature list later, if it becomed important, for now the concatenation of these strings suits us
                          /*, Option[Seq[String]]*/
                          onSuccess: (Seq[String], Seq[PurchaseData]) => Unit,
                          onError: InAppBilling.ResponseCode.Value => Unit
                           )(implicit context: Context): Unit = {

    //val map = collection.mutable.Map.empty[String, PurchaseItem /*to be created*/]
    val skus = collection.mutable.Buffer.empty[String]
    val datas = collection.mutable.Buffer.empty[PurchaseData]

    def execInAppTask(continuationToken: String = null): Unit = {
      executeInAppTask(
        //new InAppTask(
        backgroundOp(continuationToken), postOp
        //)
      )
    }

    def backgroundOp(continuationToken: String)(serviceInterface: IInAppBillingService) = {
      serviceInterface.getPurchases(
        InAppBilling.apiVersion,
        context.getPackageName,
        purchaseType.toString,
        continuationToken
      )
    }

    def postOp(bundle: Bundle): Unit = {
      val response = bundle.getInt(InAppBilling.responseCodeKey)
      InAppBilling.ResponseCode(response) match {
        case InAppBilling.ResponseCode.BILLING_RESPONSE_RESULT_OK => {
          val ownedSkus = bundle.getStringArrayList(InAppBilling.inAppPurchaseItemListKey).asScala.toSeq
          //log log "ownedSkus " + ownedSkus

          val purchaseDataList = bundle.getStringArrayList(InAppBilling.inAppPurchaseDataListKey).asScala.toSeq
          //log log "purchaseDataList " + purchaseDataList

          val purchaseDatas = purchaseDataList.map(parsePurchaseData).filter(_.isDefined).map(_.get)

          val signatureList = Option(bundle.getStringArrayList(InAppBilling.inAppDataSignatureKey)).map(_.asScala.toSeq);
          //log log "signatureList " + signatureList;

          val continuationToken = Option(bundle.getString(InAppBilling.inAppContinuationTokenKey))
          //log log "continuationToken: " + continuationToken

          skus ++= ownedSkus
          datas ++= purchaseDatas

          if (continuationToken.isDefined) {
            execInAppTask(continuationToken.get)
          } else {
            onSuccess(skus, datas)
          }
        }
        case responseCode => {
          //log log "getting purchased items has failed"
          //to do: we might later want to return here all the owned skus and safePurchase datas that were collected on previous calls
          //I mean we might have an invalid response code not on the first call but on a next one
          //for the time being we are considering that one failure means that everything failed
          onError(responseCode)
        }
      }
    }

    execInAppTask()
  }

  //version which ignored continuation token
  /*def queryPurchasedItems(purchaseType: PurchaseType.Value)
                         (op: (Seq[String], Seq[PurchaseData], Option[Seq[String]]) => Unit) {
    executeInAppTask(
      new InAppTask(
      {
        serviceInterface =>
          serviceInterface.getPurchases(
          InAppBilling.apiVersion,
          getPackageName,
          purchaseType.toString, {
            val continuationToken = null;
            continuationToken;
          }
          );
      }, {
        bundle =>
          val response = bundle.getInt(InAppBilling.responseCodeKey);
          InAppBilling.ResponseCode(response) match {
            case InAppBilling.ResponseCode.BILLING_RESPONSE_RESULT_OK => {
              val ownedSkus = bundle.getStringArrayList(InAppBilling.inAppPurchaseItemListKey).asScala.toSeq;
              log log "ownedSkus " + ownedSkus;

              val purchaseDataList = bundle.getStringArrayList(InAppBilling.inAppPurchaseDataListKey).asScala.toSeq;
              log log "purchaseDataList " + purchaseDataList;

              import PurchaseData.itsJsonFormat;
              val purchaseDatas = purchaseDataList.map(x => JsonParser(x).convertTo[PurchaseData]);

              val signatureList = Option(bundle.getStringArrayList(InAppBilling.inAppDataSignatureKey)).map(_.asScala.toSeq);
              log log "signatureList " + signatureList;

              val continuationToken = Option(bundle.getString(InAppBilling.inAppContinuationTokenKey));
              log log continuationToken;

              op(ownedSkus, purchaseDatas, signatureList);
            }
            case _ => {
              log log "getting purchased items has failed";
            }
          }
      }
      )
    );
  }*/

  protected def parsePurchaseData(jsonString: String): Option[PurchaseData] = {
    try {
      Option(jsonString).flatMap {
        str =>
          Json.fromJson(Json.parse(str))(PurchaseData.itsJsonFormat).fold(
            invalid = x => None,
            valid = Some.apply
          )
      }
    } catch {
      case e: org.codehaus.jackson.JsonParseException => None
    }
  }
}
