package com.pligor.inappgoogle

import play.api.libs.json._

/**
 * sample:
 {"title":"More From Artist Feature (Myxer Searcher)",
 "price":"0,78 €","type":"inapp",
 "description":"When you've found a special item  rest assured that this artist has more to show!",
 "productId":"feature_more_from_artist"}

 new sample:
 {"title":"bman subscription (bman)","price":"1,34 €","type":"subs","description":"costs ten times lower than printing cards. Meeting
                                   new people is priceless!","price_amount_micros":10000000,"price_currency_code":"DKK","productId":"basic_subscription"}

 WE MIGHT NEED TO ADD TWO NEW VARIABLES OF json: "price_amount_micros" and "price_currency_code"
 */
object SkuInfo {

  /*implicit val itsJsonFormat = jsonFormat[String, InAppBilling.Type.Value, String, String, String, SkuInfo](
    SkuInfo.apply,
    "productId",
    "type",
    "price",
    "title",
    "description"
  )*/

  implicit val itsJsonFormat = Json.format[SkuInfo]
}

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
case class SkuInfo(productId: String,
                   `type`: InAppBilling.Type.Value,
                   price: String,
                   title: String,
                   description: String)